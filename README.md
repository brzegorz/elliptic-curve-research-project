# Elliptic Curve Cryptosystem within Optimal Extension Fields
# State: Unfinished, left for future
Research project for Computer Systems Security on V-th semester. I've strived to implement basic cryptographics algorithms
based on Elliptic Curve Cryptography. Unfortunately, the task was too large and I haven't finished it. Very educational, though.

# Dependencies
- GNU Multiprecision Library: https://gmplib.org/
- Pari GP: https://pari.math.u-bordeaux.fr/

# How to build
  There's a make file in the src folder

# Code design
  Code tries to mirror the natural structure of used ideas. The ExtensionField objects represent parameters of particular
  Extension Fields. ExtensionFieldElements and SubfieldElements use pointers to the Extension Field they belong to.
  Points of Elliptic Curves are built upon the ExtensionFieldElements. The algebraic operations are implemented, where possible
  , with operators overload to reflect their connection to basic arithmetical operations. Then wehave Domain, which should
  contain Domain  parameters for cryptographic protocols. I've done my best to implement fastest algorithms possible, but
  there's much left to be done.
  
  For in-depth explanation and description, please see the pdf file in the main folder.
  
 # Done so far:
 - Subfield and Extension Field operations. Exponentation is sub-optimal
 - Elliptic Curve operations. If provided with SEA point counting algorithm implementations, some of those could be faster.
 - Domain parameter generation is halfway finished
 
 # TODO:
 - Extended Cornacchia algorithm(see the LyDia implementations)
 - Generate cryptographically secure domain parameters
 - Implement basic protocols
 
 # Contact
 - In case you have questions about the code or suggestions to improve it, please contact me at bogdal.grzegorz@gmail.com
 
