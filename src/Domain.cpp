/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Domain.cpp
 * Author: gbogdal
 * 
 * Created on 16 January 2017, 18:27
 */

#include "ExtensionField.h"
#include "SubfieldElement.h"
#include "Domain.h"
#include "Utils.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <pari/pari.h>
#include <cstdlib>
#include <ctime>
#include <gmp.h>

Domain::Domain() {
}

Domain::Domain(const Domain& orig) {
}

Domain::~Domain() {
}

//[B01], p.5
void Domain::findField(unsigned long long* p, unsigned long long* r, long* delta, unsigned long long n, int m, int h0){
	srand(time(NULL));
	unsigned long long t, limit, P;
    if(n > sizeof(unsigned long long)-1){
	std::cout<<"Please input smaller n for domain parameters generation\n";
	return;
	}
    limit = pow(2, n);
    P = maxP(limit, m);
    if(limit - P > sqrt(limit)){
		 std::cout<<"No OEF for this parameters";
		 return;
	}
	pari_init(500000,0);	
    int h = h0;
    while(!h % m == 0) h++;
    while(true){
		*delta = nextDiscriminant(h, 0);
        while(*delta > -60000){
			P = maxP(limit, m);
			while(limit - P < sqrt(limit)){
				t = cornacchia(*delta, P);
				if(t == 0){
					//t = cornacchiaPrimePower(*delta, P, m);
					if(t != 0)
						return;
				}
			}	
		}
    }
}

//Algorithm 1.5.3 from Cohen
unsigned long long Domain::cornacchia(long delta, unsigned long long p){
	//1.p is never 2 for this application
	unsigned long long x0, a, b, r, l;
	//2. Test is residue. Kronecker symbol is reduced to Legendre symbol cause p is an odd prime
	mpz_t Delta, P;
	mpz_init(Delta);	mpz_init(P);
	mpz_set_si(Delta, delta); Utils::ull2mpz(P, p);
	long k = mpz_legendre(Delta, P);
	if(k == -1)	return 0;
	//3. Compute square root
	x0 = shanks(delta, p);
	if(Utils::mod(x0, 2) != Utils::mod(delta, 2))	x0 -= p;
	//Mindful of overflows
	a = 2*p;
	b = x0;
	l = floor(2*sqrt(p));
	//4. Euclidean algorithm
	while(b > l){
		r = a % b;
		a = b;
		b = r;
	}
	//5. Test solution
	if(abs(delta) % (4*p - (unsigned long long)pow(b, 2)) != 0 or !Utils::isPerfectSquare((4*p - pow(b, 2))/abs(delta)))	return 0;
	return b;
}

//As p in this application is always =5(mod8), there's no need for full shank's algorithm
//Cohen, p.49
unsigned long long Domain::shanks(long a, unsigned long long p){
	unsigned long long result;
	mpz_t A, P, tmp1, tmp2, tmp3;
	mpz_init(A);	mpz_init(P);	mpz_init(tmp1);	mpz_init(tmp2);	mpz_init(tmp3);
	mpz_set_si(A, a);
	Utils::ull2mpz(P, p);
	//Check value of a^((p-1)/4)
	mpz_sub_ui(tmp2, P, 1);
	mpz_cdiv_q_ui(tmp1, tmp2, 4);	
	mpz_powm(tmp1, A, tmp1, P);
	if(mpz_cmp(tmp1, 0)){
		mpz_add_ui(tmp1, P, 3);
		mpz_cdiv_q_ui(tmp1, tmp1, 8);
		mpz_powm(tmp2, A, tmp1, P);
		mpz_abs(tmp2, tmp2);
		result = Utils::mpz2ull(tmp2);
		mpz_clear(A);	mpz_clear(P);	mpz_clear(tmp1);	mpz_clear(tmp2);	mpz_clear(tmp3);
		return result;
	}
	else{
		mpz_mul_ui(tmp1, A, 4);
		mpz_sub_ui(tmp2, P, 5);
		mpz_cdiv_q_ui(tmp2, tmp2, 8);
		mpz_powm(tmp1, tmp1, tmp2, P);
		mpz_mul_ui(tmp2, A, 2);
		mpz_mul(tmp1, tmp1, tmp2);
		mpz_mod(tmp1, tmp1, P);
		mpz_abs(tmp1, tmp1);
		result = Utils::mpz2ull(tmp1);
		mpz_clear(A);	mpz_clear(P);	mpz_clear(tmp1);	mpz_clear(tmp2);	mpz_clear(tmp3);
		return result;
	}
	
	return 0;
}

unsigned long long Domain::maxP(unsigned long long limit, int m){
    int p = prevPrime(limit);
    while(p % m != 1)   p = prevPrime(p);
    return p;
}

long Domain::nextDiscriminant(int h, long delta){
	std::ifstream database("../data/FundamentalDeltas.csv");
	std::string line;
	std::string delimiter=",";
	long potentialDelta, potentialH;
	//clean header
	std::getline(database, line);
	while(std::getline(database, line)){
		potentialDelta = stol(line.substr(0, line.find(delimiter)));
		potentialH = stol(line.substr(line.find(delimiter)));
		if(potentialH == h and potentialDelta < delta) return potentialDelta;
	}
	return 0;
}

unsigned long long Domain::prevPrime(unsigned long long p){
    int num,count=0;
    for( int a=p-1;a>=1;a--){
        for(int b=2;b<a;b++){
            if(a%b==0)
            count++;
        }
        if(count==0){
            if(a==1){
                return 0;
           }
            return a;
        }
       count=0;
    }    
}

/*
 * //NOTE: unfinished due to Legendre symbol in GMP library
//Algorithm 1.4.12 from Coheb
//NOTE: Table look-up not used
int kroneckerSymbol(long a, unsigned long long b){
	mpz_t A, B, v, k, r;
	mpz_init(A);	mpz_init(B);	mpz_init(v);	mpz_init(k);	mpz_init(r);
	mpz_set_si(A, a);	Utils::ull2mpz(B, b);
	//1. Test b==0
	if(b == 0){
		if(abs(a) != 1){
			mpz_clear(A);	mpz_clear(B);	mpz_clear(v);	mpz_clear(k);	mpz_clear(r);	
			return 0;
		}
		else{
			mpz_clear(A);	mpz_clear(B);	mpz_clear(v);	mpz_clear(k);	mpz_clear(r);			
 			return 1;
		}
	}
	//2. Remove 2's from b
	if(Utils::mod(a, 2) == 0 and b%2 ==0)	return 0;
	else{
		v = 0;
		while(b%2 == 0){
			v++;
			b>>=1;
		}	
	if(v%2 == 0)	k = 1;
	else			k = pow(-1, (pow(a,2)-1)/8);
	}
	//3. Reduce size once
	a = Utils::mod(a, b);
	//4. Finished?
	while(true){
		if(a == 0){
			if(b > 1){
				mpz_clear(A);	mpz_clear(B);	mpz_clear(v);	mpz_clear(k);	mpz_clear(r);
				return 0;
			}
			if(b == 1){
				mpz_clear(A);	mpz_clear(B);	mpz_clear(v);	mpz_clear(k);	mpz_clear(r);
				return k;
			}
		}
		//5. Remove powers of 2
		v = 0;
		while(mod(a, 2) == 0){
			v++;
			a>>=1;
		}
		//NOTE::OVERFLOW!
		if(mod(v, 2) == 1) k = pow(-1, (pow(b,2)-1)/8)*k;
		//6. Substract and apply reciprocity
		r = b - a;
		if(r > 0){
		k = pow(-1, (a-1)(b-1)/4)*k;
		b = a;
		a = r;
		}
		else 	a = -r;
	}
}
*/
