/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Utils.cpp
 * Author: gbogdal
 * 
 * Created on 16 January 2017, 18:45
 */

#include "Utils.h"
#include <gmp.h>
#include <fstream>
#include <iostream>
#include <pari/pari.h>
#include <random>


unsigned long long Utils::mpz2ull(mpz_t z){
    unsigned long long result = 0;
    mpz_export(&result, 0, -1, sizeof result, 0, 0, z);
    return result;
}

void Utils::ull2mpz(mpz_t z, unsigned long long ull){
    mpz_import(z, 1, -1, sizeof ull, 0, 0, &ull);
}

void Utils::createFundamentalDeltaDatabase(long int bound, int h0){
    std::ofstream deltaBase("../data/FundamentalDeltas.csv");
    deltaBase<<"delta, h\n";
	int h;
	long b, a, q, B;
	int delta=0;
    while(delta>=bound){
		//http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.126.7076&rep=rep1&type=pdf, p.5
        if(Utils::mod(delta, 8) == 5 and unegisfundamental(-delta)){
			//std::cout<<"Analyzing number "<<delta<<" \n"<<std::flush;
			//Computing class number by Cohen, algorithm 5.3.5
			//1. Initialize b
			h = 1;
			b = Utils::mod(delta, 2);
			B = floor(sqrt(-delta/3));
			//std::cout<<"b = "<<b<<std::endl;
			do{
				//2. Initialize a
				q = (pow(b, 2)-delta)/4;
				a = b;
				if(a <= 1)	a = 1;
				//3. Test
				else 		Utils::testReducedForm(&a, &h, b, q);
				//4. Loop on a
				while(pow(a, 2) <= q){
					a++;
					Utils::testReducedForm(&a, &h, b, q);
				 }
				//5. Loop on b
				b+=2;			
			}while(b <= B);
			if(h>=h0)	deltaBase << delta <<","<< h<<"\n";
			//else 		std::cout<<"Class number: "<<h<<"\n";
		}
	delta--;         
    }
}

void Utils::testReducedForm(long* a, int* h, long b, long q){
	if(Utils::mod(q, *a) == 0){
		if(*a == b or pow(*a, 2) == b or b == 0) *h++;
		else 	*h+=2;
	}
}

int Utils::mod(long a, unsigned long long b){
    if(b < 0) //you can check for b == 0 separately and do what you want
        return mod(a, -b);   
    int ret = a % b;
    if(ret < 0)
      ret+=b;
    return ret;
}

long long Utils::randomLL(){
	std::random_device rd;
    std::mt19937_64 e2(rd());
    std::uniform_int_distribution<long long> dist(LLONG_MIN, LLONG_MAX);
	return dist(e2);
}

bool Utils::isPerfectSquare(unsigned long long n){
    unsigned long long squareRootN=(unsigned long long)round((sqrt(n)));

    if(squareRootN*squareRootN == n) {
        return true; 
    }
     else {
        return false; 
     }
}
