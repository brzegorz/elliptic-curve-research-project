/* 
 * File:   SubfieldElement.cpp
 * Author: gbogdal
 * 
 * Created on 24 December 2016, 12:07
 */

#include "Utils.h"
#include "ExtensionField.h"
#include "SubfieldElement.h"
#include <gmp.h>
#include<iostream>
#include<vector>

    SubfieldElement::SubfieldElement(unsigned long long a, ExtensionField* field){
        this->field = field;
        value = a;
    }
    
    SubfieldElement::SubfieldElement(ExtensionField *field){
        this->field = field;
        value = 0;
    }
    
    SubfieldElement::SubfieldElement(const SubfieldElement& orig){
        this->field = orig.getField();
        this->value = orig.getValue();
    }
    
    SubfieldElement::~SubfieldElement(){
        
    }
    
    SubfieldElement SubfieldElement::operator+(SubfieldElement& a){
        mpz_t augend, addend, sum, P;
        mpz_init(augend); mpz_init(addend); mpz_init(sum); mpz_init(P);
        Utils::ull2mpz(augend, this->getValue());Utils::ull2mpz(addend, a.getValue());
        mpz_add(sum, augend, addend);
        Utils::ull2mpz(P, this->getP());
        if(mpz_cmp(sum, P) >= 0) mpz_sub(sum, sum, P);
        SubfieldElement result = SubfieldElement(Utils::mpz2ull(sum), this->getField());
        mpz_clear(augend); mpz_clear(addend); mpz_clear(sum); mpz_clear(P);        
        return result;
    }
    
    SubfieldElement SubfieldElement::operator-(SubfieldElement& a){
        mpz_t minuend, subtrahend, difference, P;
        mpz_init(minuend); mpz_init(subtrahend); mpz_init(difference); mpz_init(P);
        Utils::ull2mpz(minuend, this->getValue());Utils::ull2mpz(subtrahend, a.getValue());
        mpz_sub(difference, minuend, subtrahend);
        Utils::ull2mpz(P, this->getP());
        if(mpz_cmp(difference, 0) < 0) mpz_add(difference, difference, P);       
        SubfieldElement result(Utils::mpz2ull(difference), this->getField());
        mpz_clear(minuend); mpz_clear(subtrahend); mpz_clear(difference); mpz_clear(P);                 
        return result;
    }
    
    SubfieldElement SubfieldElement::operator-(int a){
        mpz_t minuend, subtrahend, difference, P;
        mpz_init(minuend); mpz_init(subtrahend); mpz_init(difference); mpz_init(P);
        Utils::ull2mpz(minuend, this->getValue());Utils::ull2mpz(subtrahend, (unsigned long long)a);
        mpz_sub(difference, minuend, subtrahend);
        Utils::ull2mpz(P, this->getP());
        if(mpz_cmp(difference, 0) < 0) mpz_add(difference, difference, P);       
        SubfieldElement result(Utils::mpz2ull(difference), this->getField());
        mpz_clear(minuend); mpz_clear(subtrahend); mpz_clear(difference); mpz_clear(P);                 
        return result;
    }
        
    //Note: Dependent on c==1
    //modular reduction as in p.22 of [B00]
    SubfieldElement SubfieldElement::operator*(SubfieldElement a){
        mpz_t multiplicand, multiplier, product, shiftedMul, P;
        ExtensionField* field = this->getField();
        unsigned long long n = field->getN();
        int c = this->getC();
        mpz_init(multiplicand); mpz_init(multiplier); mpz_init(product); mpz_init(shiftedMul); mpz_init(P);
        Utils::ull2mpz(multiplicand, this->getValue());Utils::ull2mpz(multiplier, a.getValue());Utils::ull2mpz(P, this->getP());
        mpz_mul(product, multiplicand, multiplier);
        //modular reduction as in p.22 of [B00]. Depends on c==1
        if(c == 1){
            if(mpz_cmp(product, P) >= 0){
                //std::cout<<"Performing modulo reduction for multiplication of "<<this->getValue()<<" and "<<a.getValue()<<"\n"<<std::flush;
                mpz_fdiv_q_2exp(shiftedMul, product, n);
                mpz_add(product, product, shiftedMul);
                mpz_mul_2exp(shiftedMul, shiftedMul, n);
                mpz_sub(product, product, shiftedMul);
            }  
        }else{  //[B00], algorithm 1
            std::vector<mpz_t> q, r;
            mpz_t tmp, R;
            int i;
            mpz_init(q[0]); mpz_init(r[0]); mpz_init(tmp);  mpz_init(R);
            
            mpz_fdiv_q_2exp(q[0], product, n);
            mpz_mul_2exp(tmp, q[0], n);
            mpz_sub(r[0], product, tmp);
            mpz_set(product, r[0]);
            i = 0;
            while(mpz_cmp(q[i], 0) > 0){
                mpz_init(q[i+i]);   mpz_init(r[i+i]);
                mpz_mul_si(tmp, q[i], c);
                mpz_fdiv_q_2exp(q[i+1], tmp, n);
                mpz_set(r[i+1], tmp);
                mpz_mul_2exp(tmp, q[i+1], n);
                mpz_sub(r[i+1], r[i+1], tmp);
                i++;
                mpz_add(product, product, r[i]);
            }
            while(i>=0){
                mpz_clear(q[i]);
                mpz_clear(r[i]);
                i--;
            }
        }
        while(mpz_cmp(product, P) >= 0) mpz_sub(product, product, P);
        unsigned long long prod;
		mpz_export(&prod, 0, -1, sizeof prod, 0, 0, product);
        SubfieldElement result(prod, field);
        mpz_clear(multiplicand); mpz_clear(multiplier); mpz_clear(product); mpz_clear(shiftedMul); mpz_clear(P);
        return result;
    }
    
    SubfieldElement SubfieldElement::operator*(int a){
        int n = this->getN();
        SubfieldElement b(a, this->getField());
        return b**this;
    }
    
    SubfieldElement SubfieldElement::operator/(SubfieldElement& a){
        ExtensionField* field = this->getField();
        return SubfieldElement(this->getValue()/a.getValue(), field);
    }
                
    bool SubfieldElement::operator==(SubfieldElement& a){
        if(this->getValue() != a.getValue()) return false;
        return true;
    }
    
    bool SubfieldElement::isAdditiveInverse(SubfieldElement a, SubfieldElement b){
        if(SubfieldElement(b + a).getValue() == 0) return true;
        return false;
    }    
    
    SubfieldElement SubfieldElement::additiveInverse(SubfieldElement a){
        return SubfieldElement(a.getP()-a.getValue(), a.getField());
    }
    
	//https://www.eng.okayama-u.ac.jp/wp-content/uploads/2016/10/42_No4.pdf algorithm 1
    SubfieldElement SubfieldElement::power(SubfieldElement a, unsigned long long exponent){
        SubfieldElement result(1, a.getField());
        SubfieldElement copy(a);
        //std::cout<<"Power called for base "<<a.getValue()<<" with exponent "<<exponent<<".\n"<<std::flush;
	while(exponent != 0){
		//std::cout<<"Current exponent value is: "<<exponent<<"\n"<<std::flush;
		if(exponent & 1) result = result * copy;
		copy = copy * copy;
		//std::cout<<"Copy value	is "<< copy.getValue()<<"\n"<<std::flush;			
		//std::cout<<"p value		is "<< copy.getP()<<"\n"<<std::flush;
		exponent >>= 1;
	}
        return result;
    }

    SubfieldElement SubfieldElement::multiplicativeInversion(SubfieldElement a){
        /*
        //partial Montgomery inversion, algorithm 2.23 from Guide to ECC
        unsigned long long u,v,x1,x2,k, p;
        u = a.getValue(); v = a.getP(); x1 = 1; x2 = 0; k = 0; p = a.getP();
        while(v > 0){
            if(v%2 == 0){ v >>= 1; x1 <<= 1;}
            else if(u%2 == 0){ u >>= 1; x2 <<= 1;}
            else if(v >= u){ v = v - u; v >>= 1; x2 += x1; x1 <<= 1;}
            else{ u = u - v; u >>= 1; x1 += x2; x2 <<= 1;}
            k++;
        }
        if(u != 1) return null;
        if(x1 > p) x1 -= p;
        //partial inversion ready
        */
        //Algorithm 2.22 from Guide to ECC
        unsigned long long u, v, x1, x2, p;
        u = a.getValue(); v = a.getP(); x1 = 1; x2 = 0; p = a.getP();
        while(u != 1 and v != 1){
            while (u%2 == 0){
                u >>= 1;
                if(x1%2 == 0) x1 >>= 1;
                else{
                    x1 += p;
                    x1 >>= 1;
                }
            }
            while (v%2 == 0){
                v >>= 1;
                if(x2%2 == 0) x2 >>= 1;
                else{
                    x2 += p;
                    x2 >>= 1;
                }
            }
            if(u >= v){u -= v; x1 -= x2;}
            else{v -= u; x2-= x1;}
        }
        if(u == 1)  return SubfieldElement(x1%p, a.getField());
        else        return SubfieldElement(x2%p, a.getField());
    }   
    
    ExtensionField* SubfieldElement::getField() const{return field;}
    
    unsigned long long SubfieldElement::getP() const{return field->getP();}
    
    int SubfieldElement::getM() const{return field->getM();}
    
    int SubfieldElement::getN() const{return field->getN();}
    
    int SubfieldElement::getC() const{return field->getC();}
    
    int SubfieldElement::getOmega() const{return field->getOmega();}
    
    void SubfieldElement::setValue(unsigned long long val){
        value = val;
    }
