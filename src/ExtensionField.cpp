/* 
 * File:   ExtensionField.cpp
 * Author: gbogdal
 * 
 * Created on 24 December 2016, 12:26
 */

#include "ExtensionField.h"
#include<iostream>
#include<vector>

ExtensionField::ExtensionField() {
    //taken from table in: [B00]. Hardcoded at the moment, ain't going
    //to use other parameters before testing
    n = 61;
    c = 1;
    p = 2305843009213693951ULL;
    m = 3;
    omega = 37;
    FrobeniusMap.reserve(this->getM()); 
    ExtensionField::calculateFrobenius(this);
}

//NOTE: m==3 required
ExtensionField::ExtensionField(int newN, int newC, unsigned long long newP, int newM, int newOmega){
	n = newN;
	c = newC;
	p = newP;
	m = newM;
	omega = newOmega;
	FrobeniusMap.reserve(this->getM());
	ExtensionField::calculateFrobenius(this);
}

ExtensionField::ExtensionField(unsigned long long newP, int newN){
	p = newP;
	n = newN;
}

ExtensionField::ExtensionField(const ExtensionField& orig) {
    p = orig.getP();
    m = orig.getM();
    n = orig.getN();
    c = orig.getC();
    omega = orig.getOmega();
    if(m==3){
       for(int i=0;i<m;i++)     FrobeniusMap[i][0] = orig.getFrobenius(i, 0);    
    }
    else if(m==6){
        FrobeniusMap.reserve(5);   
        for(int i=0;i<5;i++){
            for(int j=0;j<2;j++) FrobeniusMap[i][j] = orig.getFrobenius(i, j);
       }            
    }
}

ExtensionField::~ExtensionField() {
    FrobeniusMap.clear();
}

//Note: Dependent on m==3
void ExtensionField::calculateFrobenius(ExtensionField* field){
    SubfieldElement subOmega(field->getOmega(), field);
    unsigned long long p = field->getP();
    unsigned long long m = field->getM();
    //Calculating important for inversion instances of Frobenius map. Following
    //Guide to ECC, p.67. Yet another dependency on m==3. In case of other m,
    //FrobeniusMap variable should be a vector of vectors, and the computation
    //should be updated
    for(int j = 0; j < m; j++){
        //j*p^i mod m == j due to m being square-free
        //m is square-free. Thus, by the theorem 2.57 from Guide to Ecc,
        // p = 1(mod m). Shouldn't the multiplication be in the subfield, just in case?
        field->FrobeniusMap[j][0] = SubfieldElement::power(subOmega, ((j+1)*p/m));
        //std::cout<<"FrobeniusMap["<<j<<"][0]:"<<field->getFrobenius(j,0);
    }	
}
