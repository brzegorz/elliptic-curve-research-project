/* 
 * File:   ExtensionFieldElement.cpp
 * Author: gbogdal
 * 
 * Created on 19 December 2016, 11:58
 */

#include "Utils.h" 
#include "ExtensionFieldElement.h"
#include "ExtensionField.h"
#include "SubfieldElement.h"
#include<gmp.h>
#include<vector>
#include<iostream>


ExtensionFieldElement::ExtensionFieldElement(unsigned long long *tab, ExtensionField* field) {
    this->field = field;
    int m = field->getM();
    coefficients.reserve(m);
    for(int i=0;i<m;i++)    this->setCoefficient(i, tab[i]);
}    

ExtensionFieldElement::ExtensionFieldElement(SubfieldElement *tab, ExtensionField* field){
    this->field = field;
    int m = field->getM();
    coefficients.reserve(m);
    for(int i=0;i<m;i++)    this->setCoefficient(i, tab[i].getValue());
}
    
ExtensionFieldElement::ExtensionFieldElement(ExtensionField *field) {
    this->field = field;
    int m = field->getM();
    coefficients.reserve(m);
    for(int i=0;i<m;i++)    coefficients[i].setValue(0);
}

ExtensionFieldElement::ExtensionFieldElement(const ExtensionFieldElement& orig) {
    this->field = orig.getField();
    int m = orig.getM();
    coefficients.reserve(m);
    for(int i=0;i<m;i++)    this->setCoefficient(i, orig.getCoefficientValue(i));
}

ExtensionFieldElement::~ExtensionFieldElement() {
}
  //Addition, as explained in [BP98]
ExtensionFieldElement ExtensionFieldElement::operator+(ExtensionFieldElement& a){
    ExtensionField* field = a.getField();
    ExtensionFieldElement result(field);
    const int m = a.getM();
    const int p = a.getP();
    for(int i=0;  i < m; i++){
        result.setCoefficient(i, a.getCoefficientValue(i) + this->getCoefficientValue(i));
    }
    return result;
}

ExtensionFieldElement ExtensionFieldElement::operator-(ExtensionFieldElement a){
    ExtensionField* field = a.getField();
    ExtensionFieldElement result(field);
    const int m = a.getM();
    const int p = a.getP();
    for(int i=0;  i < m; i++){
        result.setCoefficient(i, a.getCoefficientValue(i) - this->getCoefficientValue(i));
    }
    return result;
}

//Multiplication method from [B00] and [BP98]
//Requires for c==1 and (m==3 or m==6)
ExtensionFieldElement ExtensionFieldElement::operator*(ExtensionFieldElement a){
    int m = a.getM();
    int p = a.getP();
    int n = a.getN();
    ExtensionField* field = a.getField();
    std::vector<SubfieldElement> intermediatePolynomial;
    if(m==3)       intermediatePolynomial.reserve(5);
    else if(m==6)  intermediatePolynomial.reserve(10);
    SubfieldElement resultCoefficients[m];
    obtainIntermediatePolynomial(intermediatePolynomial, a, *this);
    resultCoefficients[m-1].setValue(intermediatePolynomial[m-1].getValue());
    //(7) from [BP98]
    int omega = a.getOmega();
    for(int i=m-2;i>=0;i--){
        resultCoefficients[i].setValue((intermediatePolynomial[i+m] * omega + intermediatePolynomial[i]).getValue());
    }
    ExtensionFieldElement result(resultCoefficients, field);
    return result;
}

//As long as it's a hardcoded field, two elements are equal if their coefficients
 //are equal
ExtensionFieldElement ExtensionFieldElement::operator*(int a){
    int m = this->getM();
    ExtensionFieldElement result(this->getField());
    for(int i=0;i<m;i++){
        result.setCoefficient(i, getCoefficientValue(i)*a);
    }
    return result;
}

bool ExtensionFieldElement::operator==(ExtensionFieldElement& lElement){
    int m = lElement.getM();
    for(int i=0;i<m;i++){
        if(lElement.getCoefficientValue(i) != this->getCoefficientValue(i)) return false;
    }
    return true;
}

bool ExtensionFieldElement::operator==(ExtensionFieldElement lElement){
    int m = lElement.getM();
    for(int i=0;i<m;i++){
        if(lElement.getCoefficientValue(i) != this->getCoefficientValue(i)) return false;
    }
    return true;
}

//https://www.eng.okayama-u.ac.jp/wp-content/uploads/2016/10/42_No4.pdf algorithm 1
ExtensionFieldElement ExtensionFieldElement::power(ExtensionFieldElement a, unsigned long long exponent){
    ExtensionFieldElement copy(a);
    ExtensionField* field = a.getField();
    ExtensionFieldElement result(field);
    result.setCoefficient(0, 1);
    while(exponent!=0){
		std::cout<<"Current exponent value is: "<<exponent<<std::flush;
		if(exponent & 1) result = result * copy;
		copy = ExtensionFieldElement::square(copy);
		exponent >>= 1;
	}
    return result;
}

//Might be improved
ExtensionFieldElement ExtensionFieldElement::square(ExtensionFieldElement a){
    return a*a;
}

//[GP02], p.3. Depends on (m==3 or m==6)
 ExtensionFieldElement ExtensionFieldElement::multiplicativeInversion(ExtensionFieldElement a){
    ExtensionField* field = a.getField();
    unsigned long long p = a.getP();
    int m = a.getM();
    ExtensionFieldElement Arm1(a);
    //Step 1: A^(r-1) = A^(p^2)*A^p = A^2p*A^p for m==3. Guide to ecc, table 2.2
    Arm1.applyFrobeniusMap(1);    
    if(m==3){
        Arm1 = Arm1*a;
        Arm1.applyFrobeniusMap(1); 
    }
    else if(m==6){
        ExtensionFieldElement T2(Arm1*a);
        ExtensionFieldElement T3(T2); T3.applyFrobeniusMap(2);
        T2 = T3*T2;
        T2.applyFrobeniusMap(2);
        Arm1 = T2 * Arm1;
    }
    //Step 2: Obtaining A^r
    ExtensionFieldElement Ar(a*Arm1);
    //Step 3:Subfield inversion. Ar belongs to the subfield. Method exploits
    //theorem 2.18 from Guide to ECC
    SubfieldElement tmp(Ar.getCoefficientValue(0), field);
    tmp = SubfieldElement::multiplicativeInversion(tmp);
    ExtensionFieldElement invertedAr(field);
    invertedAr.setCoefficient(0, tmp.getValue());
    //Thus it's only non-zero coefficient is coefficients[0]
    //Step 4: Multiply (A^r)^-1 and A^(r-1)
    return invertedAr*Arm1;
 }
 
bool ExtensionFieldElement::isAdditiveInverse(ExtensionFieldElement a, ExtensionFieldElement b){
    int m = a.getM();
    for(int i=0;i<m;i++){
        if(!SubfieldElement::isAdditiveInverse(a.getCoefficient(i), b.getCoefficient(i))) return false;
    }
    return true;    
} 

ExtensionFieldElement ExtensionFieldElement::additiveInverse(ExtensionFieldElement a){
    int m = a.getM();
    ExtensionFieldElement result(a.getField());
    for(int i=0;i<m;i++){
        result.setCoefficient(i, SubfieldElement::additiveInverse(a.getCoefficient(i)).getValue());
    }
    return result;
}

//Obtaining intermediatePolynomial works only for m==3 or m==6
//Executed as explained in [B00], chapter 6
void ExtensionFieldElement::obtainIntermediatePolynomial(std::vector<SubfieldElement> &result, ExtensionFieldElement& a, ExtensionFieldElement& b){
    int m = a.getM();
    if(m==3){
        SubfieldElement resultArray[5];
        ExtensionFieldElement::obtainIntermediatePolynomialM3(resultArray, a, b);
        for(int i=0; i<5; i++)  result[i] = resultArray[i];
    }
    else if(m==6){
        unsigned long long Al[3], Ah[3], Bl[3], Bh[3], Ahl[3], Bhl[3];
        SubfieldElement E0[5], E1[5], E2[5], E102[5];
        for(int i=0; i<3; i++){
            Ah[i] = a.getCoefficientValue(i+3);
            Bh[i] = b.getCoefficientValue(i+3);
            Al[i] = a.getCoefficientValue(i);
            Bl[i] = b.getCoefficientValue(i);
            Ahl[i] = Ah[i] + Al[i];
            Bhl[i] = Bh[i] + Bl[i];
        }
        ExtensionFieldElement::obtainIntermediatePolynomialM3(E0, ExtensionFieldElement(Al, a.getField()), ExtensionFieldElement(Bl, a.getField()));        
        ExtensionFieldElement::obtainIntermediatePolynomialM3(E1, ExtensionFieldElement(Ahl, a.getField()), ExtensionFieldElement(Bhl, a.getField()));  
        ExtensionFieldElement::obtainIntermediatePolynomialM3(E2, ExtensionFieldElement(Ah, a.getField()), ExtensionFieldElement(Bh, a.getField()));
        for(int i=0;i<5;i++) E102[i] = E1[i] - E0[i] - E2[i];
        for(int i=0;i<3;i++) result[i] = E0[i];
        for(int i=3;i<5;i++) result[i] = E0[i] + E102[i];
        for(int i=5;i<7;i++) result[i] = E102[i] +E2[i];
        for(int i=7;i<10;i++)result[i] = E2[i];
    }
}

void ExtensionFieldElement::obtainIntermediatePolynomialM3(SubfieldElement (&result)[5],ExtensionFieldElement a, ExtensionFieldElement b){
        mpz_t D0,D1,D2,D3,D4,D5;
    mpz_t tmp1,tmp2,tmp3,tmp4;
    mpz_init(D0);mpz_init(D1);mpz_init(D2);mpz_init(D3);mpz_init(D4);mpz_init(D5);
    mpz_init(tmp1);mpz_init(tmp2);mpz_init(tmp3);mpz_init(tmp4);
    
    //D0 = a0*b0
    Utils::ull2mpz(tmp1, a.getCoefficientValue(0));Utils::ull2mpz(tmp2, this->getCoefficientValue(0));
    mpz_mul(D0, tmp1, tmp2);
    //D1=a1*b1
    Utils::ull2mpz(tmp1, a.getCoefficientValue(1));Utils::ull2mpz(tmp2, this->getCoefficientValue(1));
    mpz_mul(D1, tmp1, tmp2);
    //D2=a2*b2
    Utils::ull2mpz(tmp1, a.getCoefficientValue(2));Utils::ull2mpz(tmp2, this->getCoefficientValue(2));
    mpz_mul(D2, tmp1, tmp2);
    //D3=(a0+a1)(b0+b1)
    Utils::ull2mpz(tmp1, a.getCoefficientValue(0));Utils::ull2mpz(tmp2, a.getCoefficientValue(1));Utils::ull2mpz(tmp2, this->getCoefficientValue(0));Utils::ull2mpz(tmp2, this->getCoefficientValue(1));
    mpz_add(tmp1, tmp1, tmp2);mpz_add(tmp2,tmp3,tmp4);
    mpz_mul(D3, tmp1, tmp2);
    //D4=(a0+a2)(b0+b2)
    Utils::ull2mpz(tmp1, a.getCoefficientValue(0));Utils::ull2mpz(tmp2, a.getCoefficientValue(2));Utils::ull2mpz(tmp2, this->getCoefficientValue(0));Utils::ull2mpz(tmp2, this->getCoefficientValue(2));
    mpz_add(tmp1, tmp1, tmp2);mpz_add(tmp2,tmp3,tmp4);
    mpz_mul(D4, tmp1, tmp2);
    //D5=(a2+a2)(b2+b2)
    Utils::ull2mpz(tmp1, a.getCoefficientValue(1));Utils::ull2mpz(tmp2, a.getCoefficientValue(2));Utils::ull2mpz(tmp2, this->getCoefficientValue(1));Utils::ull2mpz(tmp2, this->getCoefficientValue(2));
    mpz_add(tmp1, tmp1, tmp2);mpz_add(tmp2,tmp3,tmp4);
    mpz_mul(D5, tmp1, tmp2);
    //setting the result coefficients
    //c0=D0
    result[0].setValue(Utils::mpz2ull(D0));
    //c1=D3-D1-D0
    mpz_sub(tmp1, D3, D1);
    mpz_sub(tmp1, tmp1, D0);
    result[1].setValue(Utils::mpz2ull(tmp1));
    //c2=D4-D2-D0+D1
    mpz_sub(tmp1, D4, D2);
    mpz_sub(tmp2, D1, D0);
    mpz_add(tmp1,tmp1,tmp2);
    result[2].setValue(Utils::mpz2ull(tmp1));
    //c3=D5-D1-D2
    mpz_sub(tmp1, D5, D1);
    mpz_sub(tmp1, tmp1, D2);
    result[3].setValue(Utils::mpz2ull(tmp1));
    //c4=D2
    result[4].setValue(Utils::mpz2ull(D2));
    //clear memory
    mpz_clear(D0);mpz_clear(D1);mpz_clear(D2);mpz_clear(D3);mpz_clear(D4);mpz_clear(D5);
    mpz_clear(tmp1);mpz_clear(tmp2);mpz_clear(tmp3);mpz_clear(tmp4);
}

ExtensionFieldElement ExtensionFieldElement::applyFrobeniusMap(int exponent){
    int m = this->getM();
    if(m==3){
        for(int j=0; j<m;j++){
            SubfieldElement Frob(this->getField()->getFrobenius(j, 0));
            SubfieldElement coefficient(this->getCoefficientValue(j), this->getField());
            this->setCoefficient(j, (coefficient*Frob).getValue());
        }
    }
    else if(m==6){
        for(int j=0; j<m; j++){
            if(exponent==1){
            SubfieldElement Frob(this->getField()->getFrobenius(j, 0));
            SubfieldElement coefficient(this->getCoefficientValue(j), this->getField());
            this->setCoefficient(j, (coefficient*Frob).getValue());
            }
            if(exponent==2){
            SubfieldElement Frob(this->getField()->getFrobenius(j, 1));
            SubfieldElement coefficient(this->getCoefficientValue(j), this->getField());
            this->setCoefficient(j, (coefficient*Frob).getValue());                
            }
        }
    }
}
