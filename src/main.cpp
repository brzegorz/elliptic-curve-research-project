#include <stdio.h>
#include <pari/pari.h>
#include <iostream>
#include "ExtensionFieldElement.h"
#include "ExtensionField.h"
#include "Utils.h"

int main(int argc, char**argv) {

    unsigned long long tab[6];
	pari_init(500000,0);    
    std::cout<<"Field creation\n"<<std::flush;
    ExtensionField* field = new ExtensionField();
    std::cout<<"Field created.\n"<<std::flush;
    for(int i=0; i<6; i++) tab[i]=i;   
    ExtensionFieldElement x(field);
    std::cout<<x.getCoefficientValue(0)<<" \"0\" means that constructor from a field is working\n"<<std::flush;
    ExtensionFieldElement y(tab, field);;
    std::cout<<y.getCoefficientValue(1)<<" \"1\" means that constructor from an array is working\n"<<std::flush;
    ExtensionFieldElement z(y);
    std::cout<<z.getCoefficientValue(2)<<" \"2\" means that copying constructor is working\n"<<std::flush;
    
    //Subfield operations test.
    unsigned long long p = field->getP();
    SubfieldElement subMax(p-1, field);
    SubfieldElement subOne(1, field);
    SubfieldElement subTwo(2, field);
    SubfieldElement subThree(3, field);
    SubfieldElement subHalf(1565515858, field);
    SubfieldElement tmp1(subOne);
    SubfieldElement tmp2(subOne);
    std::cout<<(subMax+subOne).getValue()<<", "<<(subMax+subTwo).getValue()<<", "<<(subOne+subTwo).getValue()<<"    0, 1, 3 means that addition works\n";
    std::cout<<p<<"\n";
    for(int i = tmp1.getN()-1; i >= 0; i--){
        tmp1 = tmp1*2;
        tmp2 = tmp2*subTwo;
    }
    std::cout<<(tmp2).getValue()<<" 1 should mean that multiplication of two SubfieldElements is working\n"<<std::flush;
    std::cout<<(tmp1).getValue()<<" 1 should mean that multiplication of SubfieldElement and integer is working\n"<<std::flush;
    std::cout<<(SubfieldElement::power(subTwo, subTwo.getN())).getValue()<<" 1 should mean that exponentiation is working\n"<<std::flush;
    std::cout<<"ExtensionFieldElement tests aren't done yet\n"<<std::flush;
    std::cout<<"value of P		 "<<p<<"\n";    
    std::cout<<"Subfield multiplication: "<<(subHalf*subHalf).getValue()<<" result below P is OK\n"<<std::flush;
    //Utils::createFundamentalDeltaDatabase(-600000, 200);
    delete field;
    return 0;
}
