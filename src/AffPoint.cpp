/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EllipticCurvePointAffine.cpp
 * Author: gbogdal
 * 
 * Created on 08 January 2017, 17:23
 */

#include "AffPoint.h"
#include<vector>

AffPoint::AffPoint() {
}

AffPoint::AffPoint(ExtensionFieldElement X, ExtensionFieldElement Y) {
    coordinates.reserve(2);
    this->setX(X);
    this->setY(Y);
}

AffPoint::AffPoint(const AffPoint& orig) {
    this->coordinates.reserve(2);
    this->setX(orig.getX());
    this->setY(orig.getY());
}

AffPoint::~AffPoint() {
}

//p.101 of Guide to ECC
AffPoint AffPoint::operator+(AffPoint a){
    if(AffPoint::isAdditiveInversion(a, *this))return AffPoint(ExtensionFieldElement(this->getParent()->getField()), ExtensionFieldElement(this->getParent()->getField()));
    ExtensionFieldElement x1(this->getX());
    ExtensionFieldElement x2(a.getX());
    ExtensionFieldElement y1(this->getY());
    ExtensionFieldElement y2(a.getY());
    ExtensionFieldElement tmp(ExtensionFieldElement::square((y2-y1)*ExtensionFieldElement::multiplicativeInversion(x2-x1)));
    ExtensionFieldElement xResult(tmp-x1-x2);
    ExtensionFieldElement yResult(tmp*(x1-xResult)-y1);
    return AffPoint(xResult, yResult);
}

AffPoint AffPoint::operator-(AffPoint a){
    return *this+AffPoint(a.getX(), ExtensionFieldElement::additiveInverse(a.getY()));
}

//Algorithm 3.31 from Guide to ECC
AffPoint AffPoint::operator*(unsigned long long k){
    std::vector<int> NAF = AffPoint::NAF(k);
    AffPoint Q(ExtensionFieldElement(this->getParent()->getField()), ExtensionFieldElement(this->getParent()->getField()));
    int l = NAF.size()-1;
    for(int i=l;i>=0;i--){
        Q = AffPoint::doble(Q);
        if(NAF[i] == 1) Q = Q + *this;
        if(NAF[i] == -1)Q = Q - *this;
    }
    return Q;
}

//p.101 of Guide to ECC
AffPoint AffPoint::doble(AffPoint a){
    ExtensionFieldElement x1(a.getX());
    ExtensionFieldElement y1(a.getY());
    ExtensionFieldElement A(a.getParent()->getA());
    ExtensionFieldElement tmp(ExtensionFieldElement::square((ExtensionFieldElement::square(x1)*3+A)*ExtensionFieldElement::multiplicativeInversion(y1*2)));
    ExtensionFieldElement xResult(tmp-(x1*2));
    ExtensionFieldElement yResult(tmp*(x1-xResult)-y1);
}

bool AffPoint::isAdditiveInversion(AffPoint A, AffPoint B){
    if(A.getX() == B.getX()){
        if(ExtensionFieldElement::isAdditiveInverse(A.getY(), B.getY())) return true;
    }
    return false;
}

//Algorithm 3.30 from Guide to ECC
std::vector<int> AffPoint::NAF(unsigned long long k){
    std::vector<int> NAF;
    int i = 0;
    while(k>=1){
        if(k%2==1){
            NAF[i] = 2 - (k%4);
            k -= 2 - (k%4);
        }
        else NAF[i] = 0;
        k>>=1;
        i++;
    }
    return NAF;
}
