/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Domain.h
 * Author: gbogdal
 *
 * Created on 16 January 2017, 18:27
 */

#ifndef DOMAIN_H
#define DOMAIN_H

class Domain {
public:
    Domain();
    Domain(const Domain& orig);
    virtual ~Domain();
    //[B01], p.6
    void findField(unsigned long long* p, unsigned long long* r, long* delta, unsigned long long n, int m, int h0);
private:
    //[B01], p.5
	////Algorithm 1.5.3 from Cohen
	//NOTE: Table look-up not used
	static unsigned long long cornacchia(long delta, unsigned long long p);
	
	//As p in this application is always =5(mod8), there's no need for full shank's algorithm
	//Cohen, p.49
	static unsigned long long shanks(long a, unsigned long long p);
    static unsigned long long maxP(unsigned long long limit, int m);
	static long nextDiscriminant(int h, long delta);
    static unsigned long long prevPrime(unsigned long long p);
	////Algorithm 1.4.12 from Cohen. Unfinished, found replacement in GMP
	//int kroneckerSymbol(long a, unsigned long long b);
};

#endif /* DOMAIN_H */

