/* 
 * File:   ElliptiCurve.h
 * Author: gbogdal
 *
 * Created on 08 January 2017, 16:59
 */


#ifndef ELLIPTICURVE_H
#define ELLIPTICURVE_H

#include "ExtensionFieldElement.h"
#include "ExtensionField.h"

class ElliptiCurve {
public:
    ElliptiCurve();
    ElliptiCurve(const ElliptiCurve& orig);
    virtual ~ElliptiCurve();
    void setA(ExtensionFieldElement A){parameters[0] = A;};
    void setB(ExtensionFieldElement B){parameters[1] = B;};
    void setField(ExtensionField* field){underlyingField = field;};
    ExtensionField* getField(){return underlyingField;};
    ExtensionFieldElement getA(){return parameters[0];};
    ExtensionFieldElement getB(){return parameters[1];};
private:
//According to Guite do ECC, p.99, if characteristic of field isn't 2 or 3,
//any e.curve E can be transformed to form y^2=x^3+ax+b. parameters[0] == a,
//parameters[1] == b
//NOTE: Point (0,0) is used as point in infinity. Thus, curve of form y^2=x^3+ax mustn't be used
    std::vector<ExtensionFieldElement> parameters;
    ExtensionField* underlyingField;
};

#endif /* ELLIPTICURVE_H */

