/* 
 * File:   ExtensionField.h
 * Author: gbogdal
 *
 * Created on 24 December 2016, 12:26
 */

#ifndef EXTENSIONFIELD_H
#define EXTENSIONFIELD_H

#include<gmp.h>
#include<vector>
#include<array>
#include"SubfieldElement.h"
class ExtensionField {
public:
    ExtensionField();
	ExtensionField(int newN, int newC, unsigned long long newP, int newM, int newOmega);
	ExtensionField(unsigned long long newP, int newN);
    ExtensionField(const ExtensionField& orig);
    virtual ~ExtensionField();
    unsigned long long getP() const{return p;};
    int getM() const{return m;};
    int getN() const{return n;};
    int getC() const{return c;};
    int getOmega() const{return omega;};
    SubfieldElement getFrobenius(int i, int j) const{return FrobeniusMap[i][j];};
    void setFrobenius(int i, int j, SubfieldElement value){FrobeniusMap[i][j] = value;};
    //Note: Dependent on m==3
    static void calculateFrobenius(ExtensionField* field);

private:
    //GF(p^m) elements are represented as polynomials of m-th degree
    //with coefficients from GF(p)
    //c needs to be 1   
    int n;
    int c;
    unsigned long long p;
    int m;
    int omega;
    std::vector<std::array<SubfieldElement, 2>> FrobeniusMap;
};
#endif /* EXTENSIONFIELD_H */

