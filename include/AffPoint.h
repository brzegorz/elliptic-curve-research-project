/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EllipticCurvePointAffine.h
 * Author: gbogdal
 *
 * Created on 08 January 2017, 17:23
 */

#ifndef ELLIPTICCURVEPOINTAFFINE_H
#define ELLIPTICCURVEPOINTAFFINE_H

#include "ExtensionFieldElement.h"
#include "ElliptiCurve.h"

//Full name: EllipticCurvePointAffine
class AffPoint {
public:
    AffPoint();
    AffPoint (ExtensionFieldElement X, ExtensionFieldElement Y);
    AffPoint(const AffPoint& orig);
    virtual ~AffPoint();
    //p.101 of Guide to ECC.
    AffPoint operator+(AffPoint a);
    AffPoint operator-(AffPoint a);
    //Algorithm 3.31 from Guide to ECC    
    AffPoint operator*(unsigned long long k);
    //p.101 of Guide to ECC
    static AffPoint doble(AffPoint a);
    static bool isAdditiveInversion(AffPoint A, AffPoint B);
    void setX(ExtensionFieldElement X){coordinates[0] = X;};
    void setY(ExtensionFieldElement Y){coordinates[1] = Y;};
    void setParent(ElliptiCurve* parent){this->parent = parent;};
    ElliptiCurve* getParent(){return parent;};
    ExtensionFieldElement getX() const{return coordinates[0];};
    ExtensionFieldElement getY() const{return coordinates[1];};
private:
    ElliptiCurve* parent;
    //coordinates[0] = x, coordinates[1] = y
    std::vector<ExtensionFieldElement> coordinates;
    //Algorithm 3.30 from Guide to ECC
    std::vector<int> NAF(unsigned long long k);
    
};

#endif /* ELLIPTICCURVEPOINTAFFINE_H */

