    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Utils.h
 * Author: gbogdal
 *
 * Created on 16 January 2017, 18:45
 */

#ifndef UTILS_H
#define UTILS_H

#include<gmp.h>

class Utils {
public:
    //Functions below are aiding the GMP library by converting it's
    //types to and from unsigned long long type
    static unsigned long long mpz2ull(mpz_t z);
    static void ull2mpz(mpz_t z, unsigned long long ull);
    static void createFundamentalDeltaDatabase(long bound, int h0);
    static int mod (long a, unsigned long long b);
    static long long randomLL();
    static bool isPerfectSquare(unsigned long long n);
private:
	static void testReducedForm(long* a, int* h, long b, long q);
};

#endif /* UTILS_H */

