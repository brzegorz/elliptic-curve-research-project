/* 
 * File:   SubfieldElement.h
 * Author: gbogdal
 *
 * Created on 24 December 2016, 12:07
 */

#ifndef SUBFIELDELEMENT_H
#define SUBFIELDELEMENT_H

class ExtensionField;

class SubfieldElement {
public:
    SubfieldElement(){};
    SubfieldElement(unsigned long long a, ExtensionField* field);
    SubfieldElement(ExtensionField* field);
    SubfieldElement(const SubfieldElement& orig);
    virtual ~SubfieldElement();
    SubfieldElement operator+(SubfieldElement& a);
    SubfieldElement operator-(SubfieldElement& a);
    SubfieldElement operator-(int a);
    //Note: Dependend on c==1
    SubfieldElement operator*(SubfieldElement a);
    SubfieldElement operator*(int a);
    SubfieldElement operator/(SubfieldElement& a);
    bool operator==(SubfieldElement& a);
    static bool isAdditiveInverse(SubfieldElement a, SubfieldElement b);
    static SubfieldElement additiveInverse(SubfieldElement a);
    static SubfieldElement power(SubfieldElement a, unsigned long long exponent);
    static SubfieldElement multiplicativeInversion(SubfieldElement a);
    unsigned long long getValue() const{return value;};
    ExtensionField* getField() const;
    unsigned long long getP() const;
    int getM() const;
    int getN() const;
    int getC() const;
    int getOmega() const;
    void setValue(unsigned long long val);
private:
    ExtensionField* field;
    unsigned long long value;
};

#endif /* SUBFIELDELEMENT_H */

