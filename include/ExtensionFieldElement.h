/* 
 * File:   ExtensionFieldElement.h
 * Author: gbogdal
 *
 * Created on 19 December 2016, 11:58
 */

//TODO: Squaring
#ifndef EXTENSIONFIELDELEMENT_H
#define EXTENSIONFIELDELEMENT_H

#include <vector>
#include<cmath>
#include<gmp.h>
#include "ExtensionField.h"
#include "SubfieldElement.h"

class ExtensionFieldElement {
public:
    //NOTE: No protection from assigning too big numbers in the construction
    ExtensionFieldElement(unsigned long long *tab, ExtensionField* field);
    ExtensionFieldElement(SubfieldElement *tab, ExtensionField* field);
    ExtensionFieldElement(ExtensionField* field);
    ExtensionFieldElement(const ExtensionFieldElement& orig);
    virtual ~ExtensionFieldElement();
    ExtensionField* getField() const{return field;};
    unsigned long long getP() const{return field->getP();};
    int getM() const{return field->getM();};
    int getN() const{return field->getN();};
    int getC() const{return field->getC();};
    int getOmega() const{return field->getOmega();};
    unsigned long long getCoefficientValue(int i) const{return coefficients[i].getValue();};
    SubfieldElement getCoefficient(int i) const{return coefficients[i];};
    void setCoefficient(int i, unsigned long long val){coefficients[i].setValue(val);};
    //Addition, as explained in [BP98]
    //Operators don't check if two elements belong to the same field.
    ExtensionFieldElement operator+(ExtensionFieldElement& a);
    ExtensionFieldElement operator-(ExtensionFieldElement a);
    //Multiplication method from [B00], works only for c==1 and (m==3 or m==6)
    ExtensionFieldElement operator*(ExtensionFieldElement a);
    //multiplication by a scalar
    ExtensionFieldElement operator*(int a);
    //No check if the field is the same
    bool operator==(ExtensionFieldElement& lElement);
    bool operator==(ExtensionFieldElement lElement);
    //https://www.eng.okayama-u.ac.jp/wp-content/uploads/2016/10/42_No4.pdf
    static ExtensionFieldElement power(ExtensionFieldElement a, unsigned long long exponent);
    static ExtensionFieldElement square(ExtensionFieldElement a);
    //[GP02], p.3. Depends on (m==3 or m==6)
    static ExtensionFieldElement multiplicativeInversion(ExtensionFieldElement a);
    static bool isAdditiveInverse(ExtensionFieldElement a, ExtensionFieldElement b);
    static ExtensionFieldElement additiveInverse(ExtensionFieldElement a);
private:
    //Important: The index of coefficient represents also the exponent, i.e
    // coefficient[0]==3, coefficients[1]=2 and all rest zeroes represent expression:
    //3*z^0 + 2*z^1
    ExtensionField* field;
    std::vector<SubfieldElement> coefficients;
    //Obtaining intermediatePolynomial works only for (m==3 or m==6)
    void obtainIntermediatePolynomial(std::vector<SubfieldElement> &result,ExtensionFieldElement& a, ExtensionFieldElement& b);
    void obtainIntermediatePolynomialM3(SubfieldElement (&result)[5],ExtensionFieldElement a, ExtensionFieldElement b);
    //Guide to ECC, p.88
    ExtensionFieldElement applyFrobeniusMap(int exponent);
};

#endif /* EXTENSIONFIELDELEMENT_H */

